﻿using System;
using System.Collections.Generic;

namespace Problem_1
{
    public class Program
    {
        public static void Main()
        {
            string[] items = new string[] { "Users", "User Groups", "User Activity Log", "Report Designer", "Report Activity Log" };
            
            //Change the search term here
            string[] filtered = filterBySearch(items, "u gr");

            for (int i = 0; i < filtered.Length; i++)
            {
                Console.WriteLine(filtered[i]);
            }

            Console.ReadLine();
        }

        public static string[] filterBySearch(string[] items, string filterSentence)
        {
            List<string> filteredList = new List<string>();
            for (int i = 0; i < items.Length; i++)
            {
                if (search(items[i], filterSentence))
                    filteredList.Add(items[i]);
            }
            string[] results = new string[filteredList.Count];
            for (int i = 0; i < filteredList.Count; i++)
            {
                results[i] = filteredList[i];
            }
            return results;
        }

        public static bool search(string sentence, string filterSentence)
        {
            string[] words = filterSentence.Split(' ');
            int currentIndex = 0;
            sentence = sentence.ToLower();

            for (int i = 0; i < words.Length; i++)
            {
                currentIndex = sentence.IndexOf(words[i], currentIndex);
                if (currentIndex == -1)
                    return false;
                currentIndex += words[i].Length;
            }

            return true;
        }
    }
}
